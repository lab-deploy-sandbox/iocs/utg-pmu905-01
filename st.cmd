##############################################################################
##
## Based on example of a startup script for one pmu905 unit from Anders
##   Sandström;
## IOC script:
##   https://github.com/anderssandstrom/ecmc_pmu905_can/blob/master/iocsh/pmu905.script
## The e3-ecmc_plugin_socketcan plugin is used to inteface to canbus:
##   https://github.com/anderssandstrom/e3-ecmc_plugin_socketcan
##
##############################################################################

##############################################################################
## Load EPICS modules
require ecmccfg 6.3.2
## Load plugin:
require ecmc_plugin_socketcan master

## Initiation:
epicsEnvSet("IOC" ,"$(IOC="IOC_TEST")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=6.3.2,MASTER_ID=-1"

##############################################################################
## Configure hardware:
# No EtherCAT hardware (in this example)..

##############################################################################
## Load plugin:
#epicsEnvSet(ECMC_PLUGIN_FILNAME,"/home/dev/epics/base-7.0.4/require/${E3_REQUIRE_VERSION}/siteMods/ecmc_plugin_socketcan/master/lib/${EPICS_HOST_ARCH=linux-x86_64}/libecmc_plugin_socketcan.so")
epicsEnvSet(ECMC_PLUGIN_FILNAME,"${ecmc_plugin_socketcan_DIR}lib/${EPICS_HOST_ARCH=linux-x86_64}/libecmc_plugin_socketcan.so")
epicsEnvSet(ECMC_PLUGIN_CONFIG,"IF=can0;DBG_PRINT=0;") # Only one option implemented in this plugin
${SCRIPTEXEC} ${ecmccfg_DIR}loadPlugin.cmd, "PLUGIN_ID=0,FILE=${ECMC_PLUGIN_FILNAME},CONFIG='${ECMC_PLUGIN_CONFIG}', REPORT=1"
epicsEnvUnset(ECMC_PLUGIN_FILNAME)
epicsEnvUnset(ECMC_PLUGIN_CONFIG)


##############################################################################
## Communication diag
dbLoadRecords(ecmcPluginSocketCAN_Com.template, "P=${ECMC_PREFIX},PORT=${ECMC_ASYN_PORT},ADDR=0,TIMEOUT=1")


##############################################################################
## PLC 0
# $(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=1000,FILE=./plc/can.plc")

##############################################################################
############# Prepare virt can for test:

# Install can utils:
#   $ git clone https://github.com/linux-can/can-utils
#   $ cd can-utils
#   $ make
#   $ make install
#
# Start virt can 0 (vcan0) and candump:
#   $ sudo modprobe vcan
#   $ sudo ip link add dev vcan0 type vcan
#   $ sudo ip link set up vcan0
#   $ candump vcan0

##############################################################################
############# Configure CANOpen plugin:

# Kvaser CANOpen USB is the Master (ID=0)
iocshLoad("iocsh/pmu905_master.iocsh", "ECMC_PREFIX=${ECMC_PREFIX},ECMC_ASYN_PORT=${ECMC_ASYN_PORT},ECMC_SAMPLE_RATE_MS=${ECMC_SAMPLE_RATE_MS},ECMC_TSE=${ECMC_TSE},DEV_ID=0")


# -----------------------------------------------------------------------------
# Configure PMU905 Nodes and adding support to their SDO:s and PDO:s
# -----------------------------------------------------------------------------
# Node-ID = 1
iocshLoad("iocsh/pmu905_slave.iocsh", "ECMC_PREFIX=${ECMC_PREFIX},ECMC_ASYN_PORT=${ECMC_ASYN_PORT},ECMC_SAMPLE_RATE_MS=${ECMC_SAMPLE_RATE_MS},ECMC_TSE=${ECMC_TSE},DEV_ID=1")
# Node-ID = 3
iocshLoad("iocsh/pmu905_slave.iocsh", "ECMC_PREFIX=${ECMC_PREFIX},ECMC_ASYN_PORT=${ECMC_ASYN_PORT},ECMC_SAMPLE_RATE_MS=${ECMC_SAMPLE_RATE_MS},ECMC_TSE=${ECMC_TSE},DEV_ID=3")

##############################################################################
############# Go to realtime:

ecmcConfigOrDie "Cfg.SetAppMode(1)"

iocInit()

dbl > pvs.log
dbl ai > ai_pvs.log
dbl bi > bi_pvs.log
dbl waveform > wf_pvs.log
dbl calc > calc_pvs.log
dbgrep  "*_"> worker_pvs.log

